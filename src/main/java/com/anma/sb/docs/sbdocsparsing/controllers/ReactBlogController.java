package com.anma.sb.docs.sbdocsparsing.controllers;

import com.anma.sb.docs.sbdocsparsing.repositories.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@Controller
@RequestMapping("/blogs/react")
public class ReactBlogController {

    @Autowired
    private BlogRepository blogRepository;


    @GetMapping
    public String getReactBlog(Model model) {

        model.addAttribute("blogs", blogRepository.findAll());

        return "react-blog";
    }

//    @GetMapping("/{blogId}")
//    public String getBlog(@PathVariable UUID blogId, Model model) {
//
//        model.addAttribute("blog", blogRepository.findById(blogId).get());
//
//        return "blog";
//    }

}
