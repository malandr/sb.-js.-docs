package com.anma.sb.docs.sbdocsparsing.services;

import com.anma.sb.docs.sbdocsparsing.models.Blog;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class DocCreationServiceImpl implements DocCreationService {

    @Override
    public void createDocument(Blog blog) throws URISyntaxException, IOException, InvalidFormatException {

        String logo = "blog-logo.png";

        // Create DOC
        XWPFDocument document = new XWPFDocument();

        // Create Title
        XWPFParagraph title = document.createParagraph();
        title.setAlignment(ParagraphAlignment.CENTER);

        XWPFRun titleRun = title.createRun();

        titleRun.setText(blog.getTitle());
        titleRun.setColor("3042E4");
        titleRun.setBold(true);
        titleRun.setFontFamily("Courier");
        titleRun.setFontSize(20);

        // Create SubTitle
        XWPFParagraph subTitle = document.createParagraph();
        subTitle.setAlignment(ParagraphAlignment.CENTER);

        XWPFRun subtitleRun = title.createRun();
        subtitleRun.setText(blog.getCreationDate().toString());

        // Insert Image
        XWPFParagraph image = document.createParagraph();
        image.setAlignment(ParagraphAlignment.CENTER);

        XWPFRun imageRun = image.createRun();
        imageRun.setTextPosition(20);

        Path imagePath = Paths.get(ClassLoader.getSystemResource(logo).toURI());

        imageRun.addPicture(Files.newInputStream(imagePath),
                XWPFDocument.PICTURE_TYPE_PNG,
                imagePath.getFileName().toString(),
                Units.toEMU(50),
                Units.toEMU(50)
        );

        XWPFParagraph body = document.createParagraph();
        body.setAlignment(ParagraphAlignment.BOTH);
        XWPFRun bodyRun = body.createRun();
        bodyRun.setText(blog.getBody());

        FileOutputStream out = new FileOutputStream(blog.getTitle());
        document.write(out);
        out.close();
        document.close();

    }
}
