package com.anma.sb.docs.sbdocsparsing.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Blog {

    @Id
    private UUID id;
    String title;
    private LocalDate creationDate;
    private String body;
    private String author;


}
