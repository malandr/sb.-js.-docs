package com.anma.sb.docs.sbdocsparsing.repositories;

import com.anma.sb.docs.sbdocsparsing.models.Blog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BlogRepository extends JpaRepository<Blog, UUID> {
}
