package com.anma.sb.docs.sbdocsparsing.config;

import com.anma.sb.docs.sbdocsparsing.models.Blog;
import com.anma.sb.docs.sbdocsparsing.repositories.BlogRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
@Log
public class Bootstrap implements CommandLineRunner {

    @Autowired
    private BlogRepository blogRepository;


    @Override
    public void run(String... args) throws Exception {
        loadData();
    }

    private void loadData() {

        Blog blog1 = Blog.builder()
                .id(UUID.fromString("4901643e-e3af-4e1f-9315-cba6e5ac98be"))
                .title("Blog 1")
                .author("Petro")
                .creationDate(LocalDate.now())
                .body("lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem" +
                        "lorem lorem lorem lorem lorem lorem lorem loremlorem lorem lorem lorem")
                .build();

        blogRepository.save(blog1);
        log.info("Blog created !!! " + blog1.getId());

        Blog blog2 = Blog.builder()
                .id(UUID.fromString("86e7b84d-0250-4895-81da-7941bf9369a5"))
                .title("Blog 2")
                .author("Vasyl")
                .creationDate(LocalDate.now())
                .body("lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem" +
                        "lorem lorem lorem lorem lorem lorem lorem loremlorem lorem lorem lorem")
                .build();

        blogRepository.save(blog2);
        log.info("Blog created !!! " + blog2.getId());

        log.info("Blogs size ==== " + String.valueOf(blogRepository.findAll().size()));
    }



}
