package com.anma.sb.docs.sbdocsparsing.services;

import com.anma.sb.docs.sbdocsparsing.models.Blog;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.IOException;
import java.net.URISyntaxException;

public interface DocCreationService {

    void createDocument(Blog blog) throws URISyntaxException, IOException, InvalidFormatException;
}
