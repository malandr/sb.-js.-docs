package com.anma.sb.docs.sbdocsparsing.controllers;

import com.anma.sb.docs.sbdocsparsing.config.Bootstrap;
import com.anma.sb.docs.sbdocsparsing.models.Blog;
import com.anma.sb.docs.sbdocsparsing.repositories.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.UUID;

@Controller
@RequestMapping("/blogs")
public class BlogController {

    @Autowired
    private BlogRepository blogRepository;


    @GetMapping
    public String getBlogs(@RequestParam(defaultValue = "10") int limit, Model model) {

        model.addAttribute("blogs", blogRepository.findAll());
        model.addAttribute("limit", limit);

        return "blogs";
    }

    @GetMapping("/{blogId}")
    public String getBlog(@PathVariable UUID blogId, Model model) {

        model.addAttribute("blog", blogRepository.findById(blogId).get());

        return "blog";
    }

}
