package com.anma.sb.docs.sbdocsparsing.controllers;

import com.anma.sb.docs.sbdocsparsing.models.Blog;
import com.anma.sb.docs.sbdocsparsing.repositories.BlogRepository;
import com.anma.sb.docs.sbdocsparsing.services.DocCreationService;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

@Controller
public class CreateDocByBlogController {

    @Autowired
    DocCreationService docCreationService;

    @Autowired
    BlogRepository blogRepository;

    @PostMapping("/create-blog/{blogId}")
    public String createBlog(@PathVariable UUID blogId, Model model) {

        try {
            Blog blog = blogRepository.findById(blogId).get();
            docCreationService.createDocument(blog);
            model.addAttribute("blog", blog);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }

        return "redirect:/blogs/" + blogId;
    }
}
