package com.anma.sb.docs.sbdocsparsing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbDocsParsingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbDocsParsingApplication.class, args);
    }

}
